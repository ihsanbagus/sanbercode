@extends('layouts/master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Pertanyaan</h3>
        <a href="{{ url('pertanyaan/create') }}" class="btn btn-success float-right">Tambah</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <table id="pertanyaan" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal Dibuat</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 200px;">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pertanyaan as $p)
                    <tr>
                        <td>{{ date('d M Y', strtotime($p->tanggal_dibuat)) }}</td>
                        <td>{{ $p->judul }}</td>
                        <td>{{ $p->isi }}</td>
                        <td>
                            <form class="float-right" action="{{ url('pertanyaan/'.$p->id) }}"
                                method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="hapus" class="btn btn-sm btn-danger">
                            </form>
                            <a href="{{ url('pertanyaan/'.$p->id.'/edit') }}"
                                class="btn btn-sm btn-warning float-right">ubah</a>
                            <a href="{{ url('pertanyaan/'.$p->id) }}"
                                class="btn btn-sm btn-primary float-right">detil</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Tanggal Dibuat</th>
                    <th>Judul</th>
                    <th>Isi</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@push('datatables')
    <script src="plugins/datatables/jquery.dataTables.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
@endpush
<script>
    $(function () {
        $("#pertanyaan").DataTable();
    });

</script>
@endsection
