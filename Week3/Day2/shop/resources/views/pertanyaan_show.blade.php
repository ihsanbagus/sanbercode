@extends('layouts/master')

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{ url()->previous() }}" class="btn btn-success">Kembali</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="callout callout-success">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    {{ $hasil->judul }}
                </div>
                <p>{{ $hasil->isi }}</p>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
