@extends('layouts/master')

@section('content')
<div class="container">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Tambah Profil</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" method="POST" action="{{ url('/profil') }}">
            @csrf
            @method('POST')
            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input autocomplete="off" name="nama_lengkap" type="text" class="form-control" id="nama" placeholder="Nama Lengkap">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input autocomplete="off" name="email" type="email" class="form-control" id="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="foto">Foto</label>
                    <input autocomplete="off" name="foto" type="foto" class="form-control" id="foto" placeholder="Foto" value="foto.jpg">
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection
