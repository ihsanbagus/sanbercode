@extends('layouts/master')

@section('content')
<div class="container">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Tambah Pertanyaan</h3>
            <a href="{{ url()->previous() }}" class="btn btn-success float-right">Kembali</a>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" method="POST" action="{{ url('pertanyaan/'.$hasil->id) }}">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input value="{{ $hasil->judul }}" autocomplete="off" name="judul" type="text" class="form-control" id="judul" placeholder="Judul">
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea class="form-control" name="isi" id="isi" rows="5" placeholder="Isi">{{ $hasil->isi }}</textarea>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection
