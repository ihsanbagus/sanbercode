<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('css/adminlte.min.css') }}">
    <script src="{{ url('plugins/jquery/jquery.min.js') }}"></script>
    <style>
        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }

    </style>
    <title>Document</title>
</head>

<body class="text-center">
    @if (session('pesan'))
        <div class="alert alert-danger">
            {{ session('pesan') }}
        </div>
    @endif
    <form class="form-signin" method="POST" action="{{ url('/login') }}">
        @csrf
        @method('POST')
        <img class="m-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" width="72"
            height="72">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required=""
            autofocus="">
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password"
            required="">
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <a href="{{ url('/register') }}" class="btn btn-lg btn-success btn-block">Register</a>
        <p class="mt-5 mb-3 text-muted">© {{ date('Y') }}</p>
    </form>

    <script src="{{ url('js/adminlte.min.js') }}"></script>
</body>

</html>
