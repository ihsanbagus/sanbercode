<!DOCTYPE html>
<html>
<body>

<h1>Buat Account Baru!</h1>

<h2>Sign Up Form</h2>

<form method="post" action="{{ url('/register') }}">
    @csrf
    @method('POST')
	<p>Name :</p>
    <input type="text" name="name"/>

	<p>Email :</p>
	<input type="email" name="email"/>

	<p>Password</p>
	<input type="password" name="password"/>
	<br/>
	<input type="submit" value="Sign Up"/>
</form>

</body>
</html>
