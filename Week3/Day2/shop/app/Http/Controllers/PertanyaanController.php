<?php

namespace App\Http\Controllers;

use App\Models\pertanyaan;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = pertanyaan::all();
        return view('pertanyaan', compact('pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pertanyaan_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'judul' => 'required',
                'isi' => 'required',
            ],
            [
                'judul.required' => 'Judul Harus Disi',
                'isi.required' => 'Isi Harus Disi'
            ]
        );
        // $pertanyaan = new pertanyaan;
        // $pertanyaan->judul = $request->judul;
        // $pertanyaan->isi = $request->isi;
        // $pertanyaan->tanggal_dibuat = date('Y-m-d');
        // $pertanyaan->profil_id = 1;
        // $pertanyaan->save();
        pertanyaan::create([
            'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_dibuat' => date('Y-m-d'),
            'profil_id' => 1
        ]);
        return redirect('/pertanyaan')->with('status', 'Berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function show(pertanyaan $pertanyaan)
    {
        $hasil = pertanyaan::find($pertanyaan->id);
        return view('pertanyaan_show', compact('hasil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function edit(pertanyaan $pertanyaan)
    {
        $hasil = pertanyaan::find($pertanyaan->id);
        return view('pertanyaan_edit', compact('hasil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pertanyaan $pertanyaan)
    {
        $validatedData = $request->validate(
            [
                'judul' => 'required',
                'isi' => 'required',
            ],
            [
                'judul.required' => 'Judul Harus Disi',
                'isi.required' => 'Isi Harus Disi'
            ]
        );
        // $pertanyaan->isi = $request->isi;
        // $pertanyaan->judul = $request->judul;
        // $pertanyaan->tanggal_diperbaharui = date('Y-m-d');
        // $pertanyaan->save();
        $pertanyaan->update([
            'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_diperbaharui' => date('Y-m-d')
        ]);
        return redirect('/pertanyaan')->with('status', 'Berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pertanyaan  $pertanyaan
     * @return \Illuminate\Http\Response
     */
    public function destroy(pertanyaan $pertanyaan)
    {
        $pertanyaan->delete();
        return redirect('/pertanyaan');
    }
}
