<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['judul', 'isi', 'tanggal_dibuat', 'profil_id'];
}
