<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Week3Day2
Route::get('/home', 'HomeController@Home')->name('home');
Route::get('/welcome', function () {
    return redirect('/home');
});
Route::post('/welcome', 'HomeController@Welcome')->name('welcome');

// Week3Day3
Route::get('/', 'HomeController@Index')->name('index');
Route::get('/data-tables', 'HomeController@DataTables');

// Week3Day5
Route::resource('/profil', 'ProfilController');
Route::resource('/pertanyaan', 'PertanyaanController');


Route::resource('/register', 'Auth\RegisterController');
