<?php
// Release 0
require_once 'Animal.php';

$sheep = new Animal("shaun");

echo $sheep->getName(); // "shaun"
echo $sheep->getLegs(); // 2
echo $sheep->getCold_blooded(); // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

// Release 1
require_once 'Ape.php';
require_once 'Frog.php';
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"