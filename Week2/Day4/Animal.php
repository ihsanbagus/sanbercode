<?php

class Animal
{
    protected $name;
    protected $legs;
    protected $cold_blooded;
    public function __construct($name)
    {
        $this->name = $name;
        $this->legs = 2;
        $this->cold_blooded = false;
    }

    /**
     * Get the value of name
     *
     * @return  mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param   mixed  $name  
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of legs
     *
     * @return  mixed
     */
    public function getLegs()
    {
        return $this->legs;
    }

    /**
     * Set the value of legs
     *
     * @param   mixed  $legs  
     *
     * @return  self
     */
    public function setLegs($legs)
    {
        $this->legs = $legs;

        return $this;
    }

    /**
     * Get the value of cold_blooded
     *
     * @return  mixed
     */
    public function getCold_blooded()
    {
        return $this->cold_blooded;
    }

    /**
     * Set the value of cold_blooded
     *
     * @param   mixed  $cold_blooded  
     *
     * @return  self
     */
    public function setCold_blooded($cold_blooded)
    {
        $this->cold_blooded = $cold_blooded;

        return $this;
    }
}
