Soal 4 Mengambil Data dari Database
a. Mengambil data users KECUALI password nya
SELECT id, name, email FROM users;

b. Mengambil data items
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name LIKE '%uniklo%';

c. Menampilkan data items join dengan kategori
SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name AS kategori FROM items i
JOIN categories c
ON i.category_id = c.id;